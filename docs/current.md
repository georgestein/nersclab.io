# Current known issues

## Perlmutter

!!! warning "Perlmutter is not a production resource"
    Perlmutter is not a production resource and usage is not charged
    against your allocation of time. While we will attempt to make the
    system available to users as much as possible, it is subject to
    unannounced and unexpected outages, reconfigurations, and periods
    of restricted access. Please visit the [timeline page](systems/perlmutter/timeline/index.md)
    for more information about changes we've made in our recent upgrades.

NERSC has automated monitoring that tracks failed nodes, so please
only open tickets for node failures if the node consistently has poor
performance relative to other nodes in the job or if the node
repeatedly causes your jobs to fail.

### New issues

- Users will encounter problems linking CUDA math libraries (cufft.h, cusolver.h, etc.)
  with any CUDAToolkit. A temporary workaround is to prepend the `$CPATH` or the
  `$CMAKE_PREFIX_PATH` (if using CMake) to point to the math_libs folder
  (note: change the cuda compiler version to match your CUDAToolkit, below is what you need for
  `cudatoolkit/21.9_11.4`:
      - to prepend your CPATH:

         ```shell
         export CPATH=/opt/nvidia/hpc_sdk/Linux_x86_64/21.9/math_libs/include:$CPATH
         ```

      - to prepend your CMake path:

         ```shell
          export CMAKE_PREFIX_PATH=/opt/nvidia/hpc_sdk/Linux_x86_64/21.9/math_libs/11.4:$CMAKE_PREFIX_PATH
         ```

- Spack on Perlmutter has gotten out-of-date with reference to the installed compilers
  and other PE components, and consequently mostly does not work at the moment. We're
  working to update the configuration.

### Ongoing issues

- MPI users may hit `segmentation fault` errors when trying
  to launch an MPI job with many ranks due to incorrect
  allocation of GPU memory. We provide [more information
  and a suggested workaround](systems/perlmutter/index.md#known-issues-with-cuda-aware-mpi).
- Some users may be unable to ssh directly to Perlmutter via
  `ssh saul-p1.nersc.gov` or `ssh perlmutter-p1.nersc.gov`, but
  are able to ssh to Perlmutter from Cori or the dtns, for example. This
  may be the result of an interaction between Cisco VPN and Sophos
  antivirus. If you have encountered this problem, please help us
  by filing a ticket. We are trying to assess how many users
  are impacted by this problem.
- Some users may see messages like `-bash:
  /usr/common/usg/bin/nersc_host: No such file or directory` when you
  login. This means you have outdated dotfiles that need to be
  updated. To stop this message, you can either delete this line from
  your dot files or check if `NERSC_HOST` is set before overwriting
  it. Please see our [environment
  page](environment/index.md#home-directories-shells-and-dotfiles)
  for more details.
- [Known issues for Machine Learning applications](machinelearning/known_issues.md)
- Nodes on Perlmutter currently do not get a constant `hostid` (IP address) response.
- `collabsu` is not available. Please create a
  [direct login](accounts/collaboration_accounts.md#direct-login) with
  [sshproxy](systems/perlmutter/index.md#connecting-to-perlmutter-with-sshproxy)
  to login into Perlmutter
  or switch to a collaboration account on Cori and then login to Perlmutter.

!!! caution "Be careful with NVIDIA Unified Memory to avoid crashing nodes"
    In your code, [NVIDIA Unified Memory](https://developer.nvidia.com/blog/unified-memory-cuda-beginners/)
    might
    look something like `cudaMallocManaged`. At the moment, we do not have
    the ability to control this kind of memory and keep it under a safe
    limit. Users who allocate a large
    pool of this kind of memory may end up crashing nodes if the UVM
    memory does not leave enough room for necessary system tools like
    our filesystem client.
    We expect a fix in early 2022. In the meantime, please keep the size
    of memory pools allocated via UVM relatively small. If you have
    questions about this, please contact us.

## Cori

The Burst Buffer on Cori has a number of known issues, documented at [Cori Burst Buffer](filesystems/cori-burst-buffer.md#known-issues).
