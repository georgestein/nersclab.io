# Julia

![Julia Logo](julia-logo.png)

Julia is a high-level, high-performance dynamic programming language for
technical computing.  It has syntax that is familiar to users of many other
technical computing environments.  Designed at MIT to tackle large-scale
partial-differential equation simulation and distributed linear algebra, Julia
features a robust ecosystem of tools for
[optimization,](https://www.juliaopt.org/)
[statistics,](https://juliastats.github.io/)
[parallel programming,](https://julia.mit.edu/#parallel) and 
[data visualization.](https://juliaplots.github.io/)
Julia is actively developed by teams
[at MIT](https://julia.mit.edu/) and 
[in industry,](https://juliacomputing.com/) along with 
[hundreds of domain-expert scientists and programmers from around the world](https://github.com/JuliaLang/julia/graphs/contributors).

## Using Julia at NERSC

Julia `1.4.2` and `1.6.0` are available at NERSC by loading the `julia` module:

```
module load julia/1.6.0
```

This also configures the `JULIA_DEPOT_PATH` and `JULIA_LOAD_PATH` to give
precedence to packages installed in the global environment.

We provide the following packages:

1. [MPI.jl](https://juliaparallel.github.io/MPI.jl/stable/)
2. [IJulia.jl](https://github.com/JuliaLang/IJulia.jl)
3. [MPIClusterManagers.jl](https://github.com/JuliaParallel/MPIClusterManagers.jl)
4. [BenchmarkTools.jl](https://github.com/JuliaCI/BenchmarkTools.jl)

which which are configured specifically for NERSC's systems. They have been
installed in the global environment `globalenv`.

Any packages you install using `Pkg.add` will be installed to the user's local
julia package directory `~/.julia`.

!!! danger "Running `Pkg.add` Will Overwrite Pre-Configured NERSC Packages"
    If you run `Pkg.add` for any packages that are already installed in
    `globalenv`, then `Pkg` will create a local install which clobbers the\
    version in `globalenv`. For convenience packages like `BenchmarkTools` this
    is not a problem. But packages that are pre-configured for NERSC's systems
    (such as MPI.jl), the local copy will not perform well -- if it runs at all.

To request new packages to be added to `globalenv` please open a [support
ticket](https://help.nersc.gov). Packages used by many users, especially those
that require specialized configuration to run on NERSC's systems, will be
considered to be included in the list above.

### Using Julia with MPI

The [MPI.jl](https://juliaparallel.github.io/MPI.jl/stable/) package has been
configured to work with `srun`. For convenience, we also include the
`mpiexecjl` wrapper. Therefore an MPI-enabled Julia program `prog.jl` can be
executed on `N` ranks either using:

```
srun -n N julia prog.jl
```

or

```
mpiexecjl -n N julia prog.jl
```

### Developing Julia Code for GPUs

Please take a look at our Perlmutter transition guide's [section on
Julia](../../performance/readiness.md#julia).

![Gadfly Demo](ResizedImage600338-gadfly-demo.png)
